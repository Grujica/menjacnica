package Menjacnica.model;

import java.util.ArrayList;

public class KusrnaLista {

	private static int brojacID = 0;
	
	protected int id;
	protected String datum;
	protected ArrayList<Valuta> valute = new ArrayList<Valuta>();
	
	public KusrnaLista() {}


	public KusrnaLista(int id, String datum) {
		if(id==0){
			brojacID++;
			id = brojacID;
		}
		this.id = id;
		this.datum = datum;
	}
	
	public KusrnaLista(int id, String datum, ArrayList<Valuta> valute) {
		super();
		this.id = id;
		this.datum = datum;
		this.valute = valute;
	}
	
	
	public KusrnaLista(String tekst){
		String [] tokeni = tekst.split(",");
			 
		
		if(tokeni.length!=2){
			System.out.println("Gre\u0161ka pri o\u010Ditavanju kursne liste "+tekst);
			//izlazak iz aplikacije
			System.exit(0);
		}
		
		id = Integer.parseInt(tokeni[0]);
		datum = tokeni[1];
		
		if (brojacID<id) {
			brojacID=id;
		}
	}
	
public String toFileRepresentation(){
		
		StringBuilder bild = new StringBuilder(); 
		bild.append(id +","+ datum);
		return bild.toString();
	}
	
	@Override
	public String toString() {
		String ispis = "Kusrna lista sa id " + id + " i datumom " 
				+ datum;
		return ispis;
	}
	
	public String toStringAllValuta() {
		StringBuilder sb = new StringBuilder("Kusrna Lista  sa id " + id + " i datumom " 
				+ datum);
		
		if(valute != null){
			sb.append(" ima vrednosti\n");
			for (int i = 0; i < valute.size(); i++) {
				sb.append("\t"+valute.get(i).toString()+"\n");
			}
		}
		return sb.toString();
		}
	
		public boolean isti(KusrnaLista kl) {
			boolean isti = false;
			if(id==kl.id)
				isti = true;
			return isti;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			KusrnaLista other = (KusrnaLista) obj;
			if (id != other.id)
				return false;
			return true;
		}


		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public String getDatum() {
			return datum;
		}


		public void setDatum(String datum) {
			this.datum = datum;
		}


		public ArrayList<Valuta> getValute() {
			return valute;
		}


		public void setValute(ArrayList<Valuta> valute) {
			this.valute = valute;
		}
	
	
		
	}
