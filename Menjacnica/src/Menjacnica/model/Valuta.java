package Menjacnica.model;

import java.util.ArrayList;


public class Valuta {

	private static int brojacID = 0;
	
	protected int id;
	protected String oznaka;
	protected String naziv;
	protected double kpVr;
	protected double prVr;
	protected ArrayList<KusrnaLista> liste = new ArrayList<KusrnaLista>();
	
	
	public Valuta() {}

	public Valuta(int id, String oznaka, String naziv, double kpVr, double prVr) {
		if(id==0){
			brojacID++;
			id = brojacID;
		}
		this.id = id;
		this.oznaka = oznaka;
		this.naziv = naziv;
		this.kpVr = kpVr;
		this.prVr = prVr;
	}

	public Valuta(int id, String oznaka, String naziv, double kpVr,
			double prVr, ArrayList<KusrnaLista> liste) {
		super();
		this.id = id;
		this.oznaka = oznaka;
		this.naziv = naziv;
		this.kpVr = kpVr;
		this.prVr = prVr;
		this.liste = liste;
	}
	
	public Valuta(String tekst){
		String [] tokeni = tekst.split(",");
		
		if(tokeni.length!=5){
			System.out.println("Gre\u0161ka pri o\u010Ditavanju Valute "+tekst);
			//izlazak iz aplikacije
			System.exit(0);
		}
		
		id = Integer.parseInt(tokeni[0]);
		oznaka = tokeni[1];
		naziv = tokeni[2];
		kpVr = Double.parseDouble(tokeni[3]);
		prVr = Double.parseDouble(tokeni[4]);
		
		if (brojacID<id) {
			brojacID=id;
		}
	}
	
       public String toFileRepresentation(){
		
		StringBuilder bild = new StringBuilder(); 
		bild.append(id +","+ oznaka+","+naziv+","+kpVr+","+prVr);
		return bild.toString();
	}
	
	@Override
	public String toString() {
		String ispis = "Valuta sa id " + id + " \u010Dija je oznaka " 
				+ oznaka + " ,naziv " + naziv + " ima kupovnu vrednost " + kpVr + " i prodajnu vrednost " + prVr;
		return ispis;
	}
	
	public String toStringAllKusrnaLista() {
		StringBuilder sb = new StringBuilder("Valuta sa id " + id + " \u010Dija je oznaka " 
				+ oznaka + " ,naziv " + naziv + " ima kupovnu vrednost " + kpVr + " i prodajnu vrednost " + prVr);
		
		if(liste != null){
			sb.append(" datuma\n");
			for (int i = 0; i < liste.size(); i++) {
				sb.append("\t"+liste.get(i).toString()+"\n");
			}
		}
		return sb.toString();
	}
	
	
	public boolean isti(Valuta vl) {
		boolean isti = false;
		if(id==vl.id)
			isti = true;
		return isti;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Valuta other = (Valuta) obj;
		if (id != other.id)
			return false;
		return true;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getOznaka() {
		return oznaka;
	}


	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public double getKpVr() {
		return kpVr;
	}


	public void setKpVr(double kpVr) {
		this.kpVr = kpVr;
	}


	public double getPrVr() {
		return prVr;
	}


	public void setPrVr(double prVr) {
		this.prVr = prVr;
	}


	public ArrayList<KusrnaLista> getListe() {
		return liste;
	}


	public void setListe(ArrayList<KusrnaLista> liste) {
		this.liste = liste;
	}

	
	

	
	
}
