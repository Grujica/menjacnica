package Menjacnica.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import Menjacnica.model.KusrnaLista;

import Menjacnica.utils.PomocnaKlasa;

public class KusrnaListaUI {

public static ArrayList<KusrnaLista> liste = new ArrayList<KusrnaLista>();
	
	/** MENI OPCJA ****/
	public static void meniKusrnaListaUI(){	
		int odluka = -1;
		while(odluka!= 0){
			ispisiTekstKusrnaListaOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz");	
					break;
				case 1:		
					ispisiSveListe();
					break;
				case 2:	
					KusrnaLista kl = pronadjiListuDatum();
					if(kl!=null){
						System.out.println(kl.toStringAllValuta());
					}	
					break;
				
				default:
					System.out.println("Nepostojeca komanda");
					break;	
			}
		}
	}
		
	public static void ispisiTekstKusrnaListaOpcije(){	
		System.out.println("Rad sa listama - opcije:");
	
		System.out.println("\tOpcija broj 1 - ispis svih lista ");
		System.out.println("\tOpcija broj 2 - ispis podataka o listi po datumu ");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");	
	}
	
	
	public static void ispisiSveListe(){
		for (int i = 0; i < liste.size(); i++) {
			System.out.println(liste.get(i));
		}
	}
	
	public static KusrnaLista pronadjiListuDatum(){
		KusrnaLista retVal = null;
		System.out.println("Unesi datum liste:");
		String klDatum = PomocnaKlasa.ocitajTekst();
		retVal = pronadjiListuDatum(klDatum);
		if(retVal == null)
			System.out.println("Lista sa datumom " +klDatum + " ne postoji u evidenciji");
		return retVal;
	}

	
	public static KusrnaLista pronadjiListuDatum(String klDatum){
		KusrnaLista retVal = null;
		for (int i = 0; i < liste.size(); i++) {
			KusrnaLista kl = liste.get(i);
			if (kl.getDatum().equals(klDatum)) {
				retVal = kl;
				break;
			}
		}
		return retVal;
	}
	
	public static KusrnaLista pronadjiListuId(int id){
		KusrnaLista retVal = null;
		for (int i = 0; i < liste.size(); i++) {
			KusrnaLista kl = liste.get(i);
			if (kl.getId()==id) {
				retVal = kl;
				break;
			}
		}
		return retVal;
	}
	
	public static int pronadjiPozicijuValuteOznaka(){
		int retVal = -1;
		System.out.println("Unesi datum:");
		String klDatum = PomocnaKlasa.ocitajTekst();
		for (int i = 0; i < liste.size(); i++) {
			KusrnaLista kl = liste.get(i);
			if (kl.getDatum().equals(klDatum)) {
				retVal = i;
				break;
			}
		}
		if(retVal == -1)
			System.out.println("Student sa oznakom " +klDatum + " ne postoji u evidenciji");
		return retVal;
	}
	
	/** METODA ZA UCITAVANJE PODATAKA****/
	static void citajIzFajlaListe(File dokument) throws IOException {
		if(dokument.exists()){

			BufferedReader in = new BufferedReader(new FileReader(dokument));
			
			//workaround for UTF-8 files and BOM marker
			//BOM (byte order mark) marker may appear on the beginning of the file
			//BOM can signal which of several Unicode encodings (8-bit, 16-bit, 32-bit) that text is encoded as
			
			in.mark(1); //zapamti trenutnu poziciju u fajlu da mozes kasnije da se vratis na nju
			if(in.read()!='\ufeff'){
				in.reset();
			}
			
			String s2;
			while((s2 = in.readLine()) != null) {
				liste.add(new KusrnaLista(s2));
			}
			in.close();
		} else {
			System.out.println("Ne postoji fajl!");
		}
	}
	
	static void pisiUFajlListe(File dokument) throws IOException {
		PrintWriter out2 = new PrintWriter(new FileWriter(dokument));
		for (KusrnaLista lista : liste) {
			out2.println(lista.toFileRepresentation());
		}
		
		out2.flush();
		out2.close();
	}
}

	


	

