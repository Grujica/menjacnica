package Menjacnica.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import Menjacnica.model.Valuta;
import Menjacnica.utils.PomocnaKlasa;
import Menjacnica.model.KusrnaLista;

public class ListaIValutaUI {

		private static void ispisiMenu() {
			System.out.println("Rad sa VALUTAMA/LISTAMA - opcije:");
			System.out.println("\tOpcija broj 1 - lista sa datumom i vrednosti za taj datum");
			System.out.println("\tOpcija broj 2 - vrednosti valuta za datum");
			System.out.println("\tOpcija broj 3 - dodavanje valute za datum");			
			System.out.println("\t\t ...");
			System.out.println("\tOpcija broj 0 - IZLAZ");
		}

		public static void menuListaIValutaUI() {
			int odluka = -1;
			while (odluka != 0) {
				ispisiMenu();
				System.out.print("opcija:");
				odluka = PomocnaKlasa.ocitajCeoBroj();
				switch (odluka) {
				case 0:
					System.out.println("Izlaz");
					break;
				case 1:
					ispisiListuDatumaZaVrednosti();
					break;
				case 2:
					ispisiVrednostiValutaZaDatum();
					break;
				case 3:
					dodavanjeValutaZaDatum();
					break;
				
				default:
					System.out.println("Nepostojeca komanda");
					break;
				}
			}
		}
		public static void ispisiListuDatumaZaVrednosti() {
			
			Valuta valuta = ValutaUI.pronadjiValutuOznaka();
			if (valuta != null) {
				List<KusrnaLista> liste = valuta.getListe();
		
				for (KusrnaLista k : liste) {
					System.out.println(k);
				}
			}
		}
		
		public static void ispisiVrednostiValutaZaDatum()  {
		
			KusrnaLista kusrnaLista = KusrnaListaUI.pronadjiListuDatum();
			if (kusrnaLista != null) {
				List<Valuta> valute = kusrnaLista.getValute();
		
				for (Valuta v : valute) {
					System.out.println(v);
				}
			}
		}

		public static void dodavanjeValutaZaDatum() {
			
		  Valuta valuta = ValutaUI.pronadjiValutuOznaka();
		
			KusrnaLista kusrnaLista = KusrnaListaUI.pronadjiListuDatum();
			
			dodavanjeValutaZaDatum(valuta, kusrnaLista);
		}

		public static void dodavanjeValutaZaDatum (Valuta valuta) {

			KusrnaLista kusrnaLista = KusrnaListaUI.pronadjiListuDatum();
			
			dodavanjeValutaZaDatum (valuta, kusrnaLista);
		}
		
		public static void dodavanjeValutaZaDatum (KusrnaLista kusrnaLista) {
			
			Valuta valuta = ValutaUI.pronadjiValutuOznaka();
			
			dodavanjeValutaZaDatum (valuta, kusrnaLista);
		}
		
		public static void dodavanjeValutaZaDatum (Valuta valuta, KusrnaLista kusrnaLista) {	
			if (valuta != null && kusrnaLista != null) {
				
				List<KusrnaLista> liste = valuta.getListe();
				boolean found = false;
				for (int i = 0; i < liste.size(); i++) {
					if(kusrnaLista.equals(liste.get(i))){
						found = true;
						break;
					}
				}
				if(!found){
					valuta.getListe().add(kusrnaLista);
					kusrnaLista.getValute().add(valuta);
				}
			}
			else {
				System.out.println("ne postoje podaci o valuti/listama");
			}
		}


		/** METODA ZA UCITAVANJE PODATAKA****/
		static void citajIzFajlaListaIValuta(File dokument) throws IOException {
			if(dokument.exists()){

				BufferedReader in = new BufferedReader(new FileReader(dokument));
				
				//workaround for UTF-8 files and BOM marker
				//BOM (byte order mark) marker may appear on the beginning of the file
				//BOM can signal which of several Unicode encodings (8-bit, 16-bit, 32-bit) that text is encoded as
				
				in.mark(1); //zapamti trenutnu poziciju u fajlu da mozes kasnije da se vratis na nju
				if(in.read()!='\ufeff'){
					in.reset();
				}
				
				String s2;
				while((s2 = in.readLine()) != null) {
					String [] ListaIValutaPodaciTekst = s2.split(",");
					int idValute = Integer.parseInt(ListaIValutaPodaciTekst[0]);
					int idListe = Integer.parseInt(ListaIValutaPodaciTekst[1]);
					Valuta vl = ValutaUI.pronadjiValutuId(idValute);
					KusrnaLista kl = KusrnaListaUI.pronadjiListuId(idListe);
					if(vl!=null && kl!=null){
						vl.getListe().add(kl);
						kl.getValute().add(vl);
					}
				}
				in.close();
			} else {
				System.out.println("Ne postoji fajl!");
			}
		}

		static void pisiUFajlListaIValuta(File dokument) throws IOException {
			PrintWriter out2 = new PrintWriter(new FileWriter(dokument));
			for (Valuta vl : ValutaUI.valute) {
				for (KusrnaLista kl : vl.getListe()) {
					out2.println(vl.getId()+","+kl.getId());
				}
			}
			
			out2.flush();
			out2.close();
		}
	}






		
		
		
