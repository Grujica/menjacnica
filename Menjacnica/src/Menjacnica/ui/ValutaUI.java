package Menjacnica.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;



import Menjacnica.ui.ListaIValutaUI;
import Menjacnica.model.Valuta;
import Menjacnica.utils.PomocnaKlasa;



@SuppressWarnings("unused")
public class ValutaUI {

public static ArrayList<Valuta> valute = new ArrayList<Valuta>();
	
	/** MENI OPCJA ****/
	public static void meniValutaUI(){	
		int odluka = -1;
		while(odluka!= 0){
			ispisiTekstValutaOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz");	
					break;
				case 1:		
					ispisiSveValute();
					break;
				case 2:	
					unosNoveValute();	
					break;
				case 3:	
					Valuta vl = pronadjiValutuOznaka();
					if(vl!=null){
						System.out.println(vl.toStringAllKusrnaLista());
					}	
					break;
				
				default:
					System.out.println("Nepostojeca komanda");
					break;	
			}
		}
	}
		
	public static void ispisiTekstValutaOpcije(){	
		System.out.println("Rad sa valutama - opcije:");
	
		System.out.println("\tOpcija broj 1 - ispis svih valuta ");
		System.out.println("\tOpcija broj 2 - unesi valutu ");
		System.out.println("\tOpcija broj 3 - ispis podataka o odre\u0111enoj valuti ");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");	
	}
	
	
	public static void ispisiSveValute(){
		for (int i = 0; i < valute.size(); i++) {
			System.out.println(valute.get(i));
		}
	}
	
	public static Valuta pronadjiValutuOznaka(){
		Valuta retVal = null;
		System.out.println("Unesi oznaku valute:");
		String vlOznaka = PomocnaKlasa.ocitajTekst();
		retVal = pronadjiValutuOznaka(vlOznaka);
		if(retVal == null)
			System.out.println("Valuta sa oznakom " +vlOznaka + " ne postoji u evidenciji");
		return retVal;
	}
	
	
	public static Valuta pronadjiValutuOznaka(String vlOznaka){
		Valuta retVal = null;
		for (int i = 0; i < valute.size(); i++) {
			Valuta vl = valute.get(i);
			if (vl.getOznaka().equals(vlOznaka)) {
				retVal = vl;
				break;
			}
		}
		return retVal;
	}
	
	public static Valuta pronadjiValutuId(int id){
		Valuta retVal = null;
		for (int i = 0; i < valute.size(); i++) {
			Valuta vl = valute.get(i);
			if (vl.getId()==id) {
				retVal = vl;
				break;
			}
		}
		return retVal;
	}
	
	public static int pronadjiPozicijuValuteOznaka(){
		int retVal = -1;
		System.out.println("Unesi oznaku:");
		String vlOznaka = PomocnaKlasa.ocitajTekst();
		for (int i = 0; i < valute.size(); i++) {
			Valuta vl = valute.get(i);
			if (vl.getOznaka().equals(vlOznaka)) {
				retVal = i;
				break;
			}
		}
		if(retVal == -1)
			System.out.println("Student sa oznakom " +vlOznaka + " ne postoji u evidenciji");
		return retVal;
	}
	
	public static void unosNoveValute(){
		System.out.print("Unesi oznaku:");
		String vlOznaka = PomocnaKlasa.ocitajTekst();
		vlOznaka = vlOznaka.toUpperCase();
		while(pronadjiValutuOznaka(vlOznaka) != null){
			System.out.println("Valuta sa oznakom "+vlOznaka + " vec postoji");
			vlOznaka = PomocnaKlasa.ocitajTekst();
		}
		System.out.print("Unesi ID:");
		int vlId = PomocnaKlasa.ocitajCeoBroj();
		System.out.print("Unesi naziv:");
		String vlNaziv = PomocnaKlasa.ocitajTekst();
		System.out.print("Unesi kupovnu vrednost:");
		double vlKpVr  = PomocnaKlasa.ocitajRealanBroj();
		System.out.print("Unesi prodajnu vrednost:");
		double vlPrVr  = PomocnaKlasa.ocitajRealanBroj();
	
		Valuta vl = new Valuta(0, vlOznaka, vlNaziv, vlKpVr, vlPrVr);
		valute.add(vl);
		
		while (PomocnaKlasa.ocitajOdlukuOPotvrdi("upisati studenta da poha\u0111a odre\u0111ene predmet") == 'Y') {
			ListaIValutaUI.dodavanjeValutaZaDatum(vl);
		}
	}
	
	/** METODA ZA UCITAVANJE PODATAKA****/
	static void citajIzFajlaValute(File dokument) throws IOException {
		if(dokument.exists()){

			BufferedReader in = new BufferedReader(new FileReader(dokument));
			
			//workaround for UTF-8 files and BOM marker
			//BOM (byte order mark) marker may appear on the beginning of the file
			//BOM can signal which of several Unicode encodings (8-bit, 16-bit, 32-bit) that text is encoded as
			
			in.mark(1); //zapamti trenutnu poziciju u fajlu da mozes kasnije da se vratis na nju
			if(in.read()!='\ufeff'){
				in.reset();
			}
			
			String s2;
			while((s2 = in.readLine()) != null) {
				valute.add(new Valuta(s2));
			}
			in.close();
		} else {
			System.out.println("Ne postoji fajl!");
		}
	}
	
	static void pisiUFajlValute(File dokument) throws IOException {
		PrintWriter out2 = new PrintWriter(new FileWriter(dokument));
		for (Valuta valuta : valute) {
			out2.println(valuta.toFileRepresentation());
		}
		
		out2.flush();
		out2.close();
	}

}