package Menjacnica.ui;

import java.io.File;
import java.io.IOException;


import Menjacnica.utils.PomocnaKlasa;



public class ApplicationUI {
	
	public static void ispisiTekstOsnovneOpcije(){	
		System.out.println("Mjenjacnica - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa valutama");
		System.out.println("\tOpcija broj 2 - Rad sa listama");
		System.out.println("\tOpcija broj 3 - Rad sa listama/valutama");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");	
	}
	
	public static void main(String[] args) throws IOException {
		String sP = System.getProperty("file.separator");
		
		File valutaFajl = new File("."+sP+"data"+sP+"valute.csv");
		ValutaUI.citajIzFajlaValute(valutaFajl);
		
		
		
		File KusrnaListaFajl = new File("."+sP+"data"+sP+"lista.csv");
		KusrnaListaUI.citajIzFajlaListe(KusrnaListaFajl);
		
		File ListaIValutaFajl = new File("."+sP+"data"+sP+"listaIvaluta.csv");
		ListaIValutaUI.citajIzFajlaListaIValuta(ListaIValutaFajl);
		
		
		int odluka = -1;
		while(odluka!= 0){
			ApplicationUI.ispisiTekstOsnovneOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz iz programa");	
					break;
				case 1:
					ValutaUI.meniValutaUI();
					break;
				case 2:
					KusrnaListaUI.meniKusrnaListaUI();
					break;
				
				case 3:
					ListaIValutaUI.menuListaIValutaUI();
					break;
				
				
				default:
					
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
		
		ValutaUI.pisiUFajlValute(valutaFajl);
		KusrnaListaUI.pisiUFajlListe(KusrnaListaFajl);
		ListaIValutaUI.pisiUFajlListaIValuta(ListaIValutaFajl);
		
		System.out.print("Program izvrsen");
	}
}
